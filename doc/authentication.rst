:orphan:

.. _index-authentication:

Authentication
==============

:doc:`SMS <authentication_sms:index>`
   Authentication per SMS.

:doc:`LDAP <ldap_authentication:index>`
   Authentication per LDAP.
