:orphan:

.. _index-marketing:

Marketing
=========

:doc:`Marketing <marketing:index>`
   Fundamentals for marketing.

:doc:`Automation <marketing_automation:index>`
   Use scenario.

:doc:`E-mail <marketing_email:index>`
   Manage mailing lists.
