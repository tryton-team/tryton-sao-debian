:orphan:

.. _index-analytic-accounting:

Analytic Accounting
===================

:doc:`Account <analytic_account:index>`
   Fundamentals for analytic.

:doc:`Invoice <analytic_invoice:index>`
   Add analytic on invoice.

:doc:`Purchase <analytic_purchase:index>`
   Add analytic on purchase.

:doc:`Sale <analytic_sale:index>`
   Add analytic on sale.
