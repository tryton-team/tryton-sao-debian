##########################
Stock Shipment Cost Module
##########################

The *Stock Shipment Cost Module* adds a shipment cost on the outgoing moves.
This cost is added to the product margin reports.
