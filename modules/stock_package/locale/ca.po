#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:stock.configuration,package_sequence:"
msgid "Package Sequence"
msgstr "Seqüència de paquet"

msgctxt "field:stock.configuration.sequence,package_sequence:"
msgid "Package Sequence"
msgstr "Seqüència de paquet"

msgctxt "field:stock.move,package:"
msgid "Package"
msgstr "Paquet"

msgctxt "field:stock.package,children:"
msgid "Children"
msgstr "Fills"

msgctxt "field:stock.package,code:"
msgid "Code"
msgstr "Codi"

msgctxt "field:stock.package,company:"
msgid "Company"
msgstr "Empresa"

msgctxt "field:stock.package,moves:"
msgid "Moves"
msgstr "Moviments"

msgctxt "field:stock.package,packaging_weight:"
msgid "Packaging Weight"
msgstr "Pes del paquet"

msgctxt "field:stock.package,packaging_weight_digits:"
msgid "Packaging Weight Digits"
msgstr "Dígits de pes del paquet"

msgctxt "field:stock.package,packaging_weight_uom:"
msgid "Packaging Weight Uom"
msgstr "UdM del pes del paquet"

msgctxt "field:stock.package,parent:"
msgid "Parent"
msgstr "Pare"

msgctxt "field:stock.package,shipment:"
msgid "Shipment"
msgstr "Albarà"

msgctxt "field:stock.package,state:"
msgid "State"
msgstr "Estat"

msgctxt "field:stock.package,type:"
msgid "Type"
msgstr "Tipus"

msgctxt "field:stock.package.type,name:"
msgid "Name"
msgstr "Nom"

msgctxt "field:stock.package.type,packaging_weight:"
msgid "Packaging Weight"
msgstr "Pes del paquet"

msgctxt "field:stock.package.type,packaging_weight_digits:"
msgid "Packaging Weight Digits"
msgstr "Dígits del pes del paquet"

msgctxt "field:stock.package.type,packaging_weight_uom:"
msgid "Packaging Weight Uom"
msgstr "UdM del pes del paquet"

msgctxt "field:stock.shipment.in.return,packages:"
msgid "Packages"
msgstr "Paquets"

msgctxt "field:stock.shipment.in.return,root_packages:"
msgid "Packages"
msgstr "Paquets"

msgctxt "field:stock.shipment.out,packages:"
msgid "Packages"
msgstr "Paquets"

msgctxt "field:stock.shipment.out,root_packages:"
msgid "Packages"
msgstr "Paquets"

msgctxt "help:stock.package,packaging_weight:"
msgid "The weight of the package when empty."
msgstr "El pes del paquet quan està buit."

msgctxt "help:stock.package.type,packaging_weight:"
msgid "The weight of the package when empty."
msgstr "El pes del paquet quan està buit."

msgctxt "model:ir.action,name:act_package_type_form"
msgid "Package Types"
msgstr "Tipus de paquet"

msgctxt "model:ir.action,name:report_package_label"
msgid "Package Labels"
msgstr "Etiquetes de paquet"

msgctxt "model:ir.message,text:msg_package_mismatch"
msgid "To process shipment \"%(shipment)s\", you must pack all its moves."
msgstr ""
"Per a processar l'albarà \"%(shipment)s\", heu d'empaquetar tots els seus "
"moviments."

msgctxt "model:ir.rule.group,name:rule_group_package_companies"
msgid "User in companies"
msgstr "Usuari a les empreses"

msgctxt "model:ir.sequence,name:sequence_package"
msgid "Stock Package"
msgstr "Paquet"

msgctxt "model:ir.sequence.type,name:sequence_type_package"
msgid "Stock Package"
msgstr "Paquet"

msgctxt "model:ir.ui.menu,name:menu_package_form"
msgid "Package Types"
msgstr "Tipus de paquet"

msgctxt "model:stock.package,name:"
msgid "Stock Package"
msgstr "Paquet"

msgctxt "model:stock.package.type,name:"
msgid "Stock Package Type"
msgstr "Tipus de paquet"

msgctxt "report:stock.package.label:"
msgid "Package:"
msgstr "Paquet:"

msgctxt "report:stock.package.label:"
msgid "Shipment:"
msgstr "Albarà:"

msgctxt "selection:stock.package,state:"
msgid "Closed"
msgstr "Tancat"

msgctxt "selection:stock.package,state:"
msgid "Open"
msgstr "Obert"

msgctxt "view:stock.package.type:"
msgid "Measurements"
msgstr "Mesures"

msgctxt "view:stock.package:"
msgid "Measurements"
msgstr "Mesures"
